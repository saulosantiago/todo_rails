[ ![Codeship Status for saulosantiago/todo_rails](https://codeship.io/projects/1de99740-478f-0132-974f-0642bcf4b40a/status)](https://codeship.io/projects/45647)

# README #

### How to run the application ###

- gem install

````
gem install bundler && bundle
````

- creating and populating the database

````
rake db:populate
````

- run server

````
rails s
````

### Test execute ###

````
rspec -fd
````