# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141102133827) do

  create_table "cards", force: true do |t|
    t.integer  "list_id"
    t.string   "name"
    t.string   "description"
    t.datetime "closed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cards", ["closed_at"], name: "index_cards_on_closed_at", using: :btree
  add_index "cards", ["list_id"], name: "index_cards_on_list_id", using: :btree

  create_table "checklists", force: true do |t|
    t.integer  "card_id"
    t.boolean  "resolved"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "checklists", ["card_id", "resolved"], name: "index_checklists_on_card_id_and_resolved", using: :btree
  add_index "checklists", ["card_id"], name: "index_checklists_on_card_id", using: :btree
  add_index "checklists", ["resolved"], name: "index_checklists_on_resolved", using: :btree

  create_table "favorite_lists", force: true do |t|
    t.integer  "list_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorite_lists", ["list_id", "user_id"], name: "index_favorite_lists_on_list_id_and_user_id", unique: true, using: :btree
  add_index "favorite_lists", ["list_id"], name: "index_favorite_lists_on_list_id", using: :btree

  create_table "lists", force: true do |t|
    t.integer  "user_id"
    t.boolean  "only_me",    default: false
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lists", ["only_me", "user_id"], name: "index_lists_on_only_me_and_user_id", using: :btree
  add_index "lists", ["only_me"], name: "index_lists_on_only_me", using: :btree
  add_index "lists", ["user_id"], name: "index_lists_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
