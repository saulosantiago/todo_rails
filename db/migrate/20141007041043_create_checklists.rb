class CreateChecklists < ActiveRecord::Migration
  def change
    create_table :checklists do |t|
      t.references :card
      t.boolean :resolved
      t.string :description
      t.timestamps
    end

    add_index :checklists, :card_id
    add_index :checklists, :resolved
    add_index :checklists, [:card_id, :resolved]
  end
end
