class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.references :list
      t.string :name
      t.string :description
      t.datetime :closed_at
      t.timestamps
    end

    add_index :cards, :list_id
    add_index :cards, :closed_at
  end
end
