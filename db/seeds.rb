10.times do
  User.create({
    email: Faker::Internet.email,
    password: '123123',
    password_confirmation: '123123'
  })
end

40.times do
  List.create({
    only_me: [true, false].sample,
    name: Faker::App.name,
    user: User.order("rand()").first
  })
end

80.times do
  Card.create({
    name: Faker::App.name,
    description: Faker::Lorem.sentence(3, false, 4),
    list: List.order("rand()").first
  })
end

120.times do
  Checklist.create({
    resolved: [true, false].sample,
    description: Faker::Lorem.sentence(3, false, 4),
    card: Card.order("rand()").first
  })
end

List.limit(10).each do |list|
  FavoriteList.create({
    list: list,
    user: User.order("rand()").first
  })
end