module CardsHelper
  def setup_card(card)
    card.tap do |r|
      r.checklists.build if r.checklists.blank?
    end
  end
end
