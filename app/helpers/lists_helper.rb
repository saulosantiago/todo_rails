module ListsHelper
  def setup_list(list)
    list.tap do |r|
      r.cards.build if r.cards.blank?
    end
  end

  def is_my(list)
    list.user == current_user ? 'is_my' : ''
  end
end
