module ApplicationHelper
  def checked_class_name(checklist)
    checklist.resolved ? 'checked' : ''
  end

  def disabled_class_name(card)
    card.closed_at ? 'disabled' : ''
  end

  def errors_wrapper(record)
    if record.errors.present?
      content_tag :div, class: 'alert alert-warning padding-10 margin-tb-10' do
        content_tag :ul, class: 'padding-rl-20' do
          record.errors.full_messages.map do |message|
            content_tag(:li) { message }
          end.join('').html_safe
        end
      end
    end
  end

  def notices_wrapper
    if notice
      content_tag :div, class: 'alert alert-info' do
        content_tag(:p, class: 'notice') { notice }
      end
    end
  end

  def alerts_wrapper
    if alert
      content_tag :div, class: 'alert alert-warning' do
        content_tag(:p, class: 'alert') { alert }
      end
    end
  end

  def nav_partial
    user_signed_in? ? 'shared/menu_logged' : 'shared/menu_unlogged'
  end
end
