class ListsController < ApplicationController
  before_action :can_handler!, only: [:edit, :update, :destroy]

  def index
    @lists = ListsPresenter.new List.publics(current_user)
  end

  def show
    @list = ListPresenter.new List.find(params[:id])
  end

  def new
    @list = current_user.lists.new
  end

  def create
    @list = current_user.lists.new permit_params

    if @list.save
      redirect_to list_path(@list)
    else
      render action: :new
    end
  end

  def edit
    @list = List.find params[:id]
  end

  def update
    @list = List.find params[:id]

    if @list.update_attributes permit_params
      redirect_to list_path(@list)
    else
      render action: :edit
    end
  end

  def destroy
    @list = List.find(params[:id])

    if @list.destroy
      redirect_to lists_path
    else
      render action: :edit
    end
  end

  def favorite
    list = List.find(params[:id])
    favorite = current_user.find_favorite_list(list)

    if favorite.present?
      favorite.first.destroy
    else
      current_user.favorite_lists.new(list_id: params[:id]).save
    end

    redirect_to lists_path
  end

  def favorites
    @lists = ListsPresenter.new(List.favorites(current_user))
  end

  def stream
    dates = List.favorites(current_user).map do |list|
      list.updated_at.strftime('%d/%m/%Y %I:%M:%S')
    end

    build_stream(dates, lists_path)
  end

  def show_stream
    dates = List.find(params[:id]).updated_at.strftime('%d/%m/%Y %I:%M:%S')
    build_stream([dates], list_path(params[:id]))
  end

  private
  def permit_params
    params.require(:list).permit(:only_me, :name, :cards_attributes => [:name, :description, :id, :_destroy])
  end

  def can_handler!
    list = List.find(params[:id])

    if current_user != list.user
      redirect_to list_path(list), alert: "You don't own this record"
    end
  end
end
