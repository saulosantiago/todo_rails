class CardsController < ApplicationController
  before_action :can_handler!, only: [:update, :destroy, :close]

  def show
    @card = CardPresenter.new Card.find params[:id]
  end

  def update
    @card = CardPresenter.new Card.find params[:id]

    if @card.get_subject.update_attributes permit_params
      redirect_to card_path(@card.get_subject)
    else
      render action: :show
    end
  end

  def destroy
    card = Card.find params[:id]

    if card.destroy
      redirect_to list_path(card.list)
    else
      render action: :show
    end
  end

  def close
    card = Card.find params[:id]

    if card.close
      redirect_to card_path(card)
    else
      render action: :show
    end
  end

  def show_stream
    dates = Card.find(params[:id]).updated_at.strftime('%d/%m/%Y %I:%M:%S')
    build_stream([dates], card_path(params[:id]))
  end

  private
  def permit_params
    params.require(:card).permit(:name, :description, :updated_at, :checklists_attributes => [:resolved, :description, :id, :_destroy])
  end

  def can_handler!
    card = Card.find(params[:id])

    if current_user != card.list.user
      redirect_to card_path(card), alert: "You don't own this record"
    end
  end
end
