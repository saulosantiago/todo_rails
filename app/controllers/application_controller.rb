class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  private
  def build_stream(dates, path)
    response.headers['Content-Type'] = "text/event-stream\n\n"

    begin
      response.stream.write "id: #{ rand(0..999999) }\n"
      response.stream.write "retry: 10000\n"
      response.stream.write "event: updates\n"
      response.stream.write "data: #{ JSON.dump({ dates: dates, path: path }) }\n\n"
    rescue IOError
      p IOError
    ensure
      response.stream.close
    end
  end
end