class Checklist < ActiveRecord::Base
  belongs_to :card

  scope :resolved, -> { where('resolved=? ', true) }

  validates_presence_of :description, :card
end