class FavoriteList < ActiveRecord::Base
  belongs_to :list
  belongs_to :user

  validates_presence_of :list, :user

  scope :by_list, -> (list) { where(list: list) }
  scope :by_user, -> (user) { where(user: user) }
end
