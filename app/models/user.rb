class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :favorite_lists, dependent: :destroy
  has_many :lists, dependent: :destroy

  def gravatar
    "http://www.gravatar.com/avatar/#{ Digest::MD5.hexdigest(email) }"
  end

  def find_favorite_list(list)
    favorite_lists.by_list(list)
  end
end
