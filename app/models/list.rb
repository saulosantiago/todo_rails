class List < ActiveRecord::Base
  has_many :cards, dependent: :destroy
  has_many :favorite_lists, dependent: :destroy
  belongs_to :user

  validates_presence_of :name, :user
  validates_uniqueness_of :name, scope: :user_id
  validates :only_me, inclusion: { in: [true, false] }

  scope :publics, -> (user) { where('only_me=? OR user_id=?', false, user).priority(user) }
  scope :priority, -> (user) { order("CASE WHEN user_id = #{ user.id } then 1 else 0 END DESC, updated_at DESC") }
  scope :favorites, -> (user) { joins(:favorite_lists).where('favorite_lists.user_id = ?', user) }

  accepts_nested_attributes_for :cards, allow_destroy: true

  def has_favorited?(user)
    favorite_lists.by_user(user).present?
  end
end