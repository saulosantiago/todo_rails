class Card < ActiveRecord::Base
  has_many :checklists, dependent: :destroy do
    def progress
      "#{ resolved.length } / #{ length }"
    end
  end
  belongs_to :list

  default_scope -> { order('CASE WHEN closed_at IS NOT NULL then 0 else 1 END DESC, created_at DESC') }

  validates_presence_of :name, :description
  validates_uniqueness_of :name, scope: :list_id, on: :create

  accepts_nested_attributes_for :checklists, allow_destroy: true

  after_save :set_closed_at

  def close
    checklists.each do |checklist|
      checklist.update_attributes(resolved: true)
    end

    set_closed_at
  end

  def closed?
    closed_at.present?
  end

  protected
  def set_closed_at
    return if checklists.length == 0

    time = if checklists.resolved.length == checklists.length
       Time.now
    else
      nil
    end

    update_columns(closed_at: time, updated_at: Time.now)
  end
end