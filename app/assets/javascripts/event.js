window.Event = (function() {
  function Event(dates, pathStream) {
    this.dates = dates;
    this.pathStream = pathStream;
    this.bind();
  }

  Event.prototype.bind = function() {
    window.source = new EventSource(this.pathStream);
    var _self = this;

    source.onopen = function(e) {
      this.addEventListener('updates', function(e){
        data = JSON.parse(e.data);
        _self.handlerReloadDiv(data)
      });
    }
  };

  Event.prototype.handlerReloadDiv = function(data) {

    $.each(data.dates, function(i, date) {
      now = new Date(date);
      old = new Date(this.dates[i]);

      if (String(now) !== String(old)) {
        $('.reload')
          .attr('href', data.path)
          .removeClass('hidden')
      } else {
        $('.reload').addClass('hidden')
      }
    }.bind(this));
  };

  return Event;
})();

