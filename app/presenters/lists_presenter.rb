class ListsPresenter < Presenter
  def get_subject
    @subject
  end

  def list_partial
    @subject.present? ? 'lists/list' : 'shared/warning'
  end

  def updates_favorite_lists(current_user)
    List.favorites(current_user).map do |list|
      list.updated_at.strftime('%d/%m/%Y %I:%M:%S')
    end.to_json
  end

  def favorite_button(current_user, list)
    if list.has_favorited?(current_user)
      h.content_tag(:a, 'del favorite', href: r.favorite_list_path(list), class: 'btn btn-danger')
    else
      h.content_tag(:a, 'add favorite', href: r.favorite_list_path(list), class: 'btn btn-success')
    end
  end
end