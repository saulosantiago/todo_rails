class ListPresenter < Presenter
  expose :id, :name, :updated_at, :user, :cards

  def card_partial
    @subject.cards.present? ? 'lists/cards_list' : 'shared/warning'
  end

  def edit_button(current_user)
    if @subject.user == current_user
      h.content_tag(:a, 'edit', href: r.edit_list_path(@subject), class: 'btn btn-primary pull-right')
    end
  end
end