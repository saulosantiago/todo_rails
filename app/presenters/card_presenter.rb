class CardPresenter < Presenter
  expose :id, :name, :description, :updated_at, :checklists, :list

  def get_subject
    @subject
  end

  def closed_label
    if @subject.closed_at
      h.content_tag(:span, class: 'label label-warning pull-right margin-tb-30') do
        'closed'
      end
    end
  end

  def checklist_partial(checklist)
    checklist.new_record? ? 'cards/new_checklist' : 'cards/checklist'
  end
end