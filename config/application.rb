require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Todo
  class Application < Rails::Application
    config.filter_parameters += [:password, :password_confirmation]
    config.autoload_paths << config.root.join("app/presenters").to_s
  end
end
