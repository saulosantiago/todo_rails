Rails.application.routes.draw do
  devise_for :users
  root to: 'lists#index'

  resources :lists do
    member do
      get :favorite
      get :show_stream
    end

    collection do
      get :stream
      get :favorites
    end
  end

  resources :cards, only: [:show, :update, :destroy] do
    member do
      get :close
      get :show_stream
    end
  end
end
