module RequestMacros
  include Warden::Test::Helpers

  def sign_in_as_a_user
    login_as create(:user)
  end
end