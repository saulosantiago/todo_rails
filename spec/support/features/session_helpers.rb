module Features
  module SessionHelpers
    def sign_up_with(email, password)
      visit new_user_registration_path
      fill_in 'email', with: email
      fill_in 'password', with: password
      fill_in 'password confirmation', with: password
      click_button 'Sign up'
    end

    def sign_in_with(email, password)
      visit new_user_session_path
      fill_in 'email', with: email
      fill_in 'password', with: password
      click_button 'Log in'
    end
  end
end