module Features
  module ListHelpers
    def fill_form_list_with_card(list)
      fill_in 'name', with: list[:name]
      fill_in 'card name', with: 'foo'
      fill_in 'card description', with: 'bar'
      click_button 'save'
    end

    def fill_form_list_with_cards(list)
      click_link 'add card'

      fill_in 'name', with: list[:name]
      page.all(:fillable_field, 'card name').first.set('foo')
      page.all(:fillable_field, 'card description').first.set('bar')
      page.all(:fillable_field, 'card name').last.set('bar')
      page.all(:fillable_field, 'card description').last.set('foo')

      click_button 'save'
    end

    def fill_form_list_without_card(list)
      click_link 'remove'

      fill_in 'name', with: list[:name]
      click_button 'save'
    end
  end
end