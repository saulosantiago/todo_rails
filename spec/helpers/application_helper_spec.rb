require "spec_helper"

describe ApplicationHelper, type: :helper do
  describe "#checked_class_name" do
    let(:checklist_resolved) { create(:checklist, resolved: true) }
    let(:checklist_non_resolved) { create(:checklist, resolved: false) }
    it { expect(helper.checked_class_name(checklist_resolved)).to be_eql('checked') }
    it { expect(helper.checked_class_name(checklist_non_resolved)).to be_eql("") }
  end

  describe '#disabled_class_name' do
    let(:checklist_closed) { create(:card, closed_at: Time.now) }
    let(:checklist_non_closed) { create(:card) }
    it { expect(helper.disabled_class_name(checklist_closed)).to be_eql('disabled') }
    it { expect(helper.disabled_class_name(checklist_non_closed)).to be_eql("") }
  end

  describe '#disabled_class_name' do
    let(:checklist_closed) { create(:card, closed_at: Time.now) }
    let(:checklist_non_closed) { create(:card) }
    it { expect(helper.disabled_class_name(checklist_closed)).to be_eql('disabled') }
    it { expect(helper.disabled_class_name(checklist_non_closed)).to be_eql("") }
  end

  describe '#errors_wrapper' do
    let(:card) { Card.new(attributes_for(:card, name: nil)) }
    before { card.save }
    it { expect(helper.errors_wrapper(card)).to match(/div/) }
    it { expect(helper.errors_wrapper(card)).to match(/ul/) }
    it { expect(helper.errors_wrapper(card)).to match(/li/) }
  end

  describe '#alerts_wrapper' do
    before { flash[:alert] = "bar" }
    it { expect(helper.alerts_wrapper).to match(/div/) }
    it { expect(helper.alerts_wrapper).to match(/alert/) }
    it { expect(helper.alerts_wrapper).to match(/bar/) }
  end

  describe '#notices_wrapper' do
    before { flash[:notice] = "foo" }
    it { expect(helper.notices_wrapper).to match(/div/) }
    it { expect(helper.notices_wrapper).to match(/notice/) }
    it { expect(helper.notices_wrapper).to match(/foo/) }
  end

  describe '#nav_partial' do
    describe 'return shared/menu_logged', type: :controller do
      before { sign_in(create(:user)) }
      it { expect(helper.nav_partial).to eql('shared/menu_logged') }
    end

    describe 'return shared/menu_unlogged', type: :controller do
      it { expect(helper.nav_partial).to eql('shared/menu_unlogged') }
    end
  end
end