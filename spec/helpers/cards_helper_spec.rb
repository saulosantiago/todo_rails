require "spec_helper"

describe CardsHelper, type: :helper do
  describe "#setup_card" do
    let(:card) { build(:card) }
    it { expect(helper.setup_card(card).checklists.first).to be_a(Checklist) }
  end
end