require "spec_helper"

describe ListsHelper, type: :helper do
  describe "#setup_list" do
    let(:list) { build(:list) }
    it { expect(helper.setup_list(list).cards.first).to be_a(Card) }
  end

  describe "#is_my" do

    describe 'others list', type: :controller do
      let(:list) { create(:list) }

      before do
        sign_in(create(:user, email: 'foo@bar.com'))
        list
      end

      it { expect(helper.is_my(list)).to be_eql('') }
    end

    describe 'my list', type: :controller do
      let(:user) { create(:user) }
      let(:list) { create(:list, user: user) }

      before do
        list
        sign_in(user)
      end

      it { expect(helper.is_my(list)).to be_eql('is_my') }
    end
  end
end