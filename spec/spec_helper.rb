require 'spork'

Spork.prefork do
  ENV['RACK_ENV'] ||= 'test'
  require File.expand_path('../../config/environment', __FILE__)
  require 'rspec/rails'
  require 'capybara/rspec'
  Dir['./spec/support/**/*.rb'].each { |f| require f }

  RSpec.configure do |config|
    config.include FactoryGirl::Syntax::Methods
    config.include Devise::TestHelpers, type: :controller
    config.include ControllerHelpers, type: :controller
    config.include Features::SessionHelpers, type: :feature
    config.include Features::ListHelpers, type: :feature
    config.include RequestMacros, type: :feature
    config.include Devise::TestHelpers, type: :view
    config.include Capybara::DSL, type: :request
    config.use_transactional_fixtures = false

    config.before :suite do
      DatabaseCleaner.strategy = :truncation
    end

    config.before :each do
      DatabaseCleaner.clean
    end
  end
end

Capybara.javascript_driver = :webkit