require 'spec_helper'

describe 'cards/show.html.erb', type: :view do
  let(:user) { create(:user) }
  let(:list) { create(:list, user: user) }
  let(:card) { create(:card, list: list) }
  let(:checklist) { create(:checklist, card: card) }

  before do
    sign_in(user)
  end

  describe 'card with checklist' do
    before do
      checklist
      assign(:card, CardPresenter.new(card))
      render
    end

    it { expect(view).to render_template(partial: '_checklists')  }
    it { expect(rendered).to have_selector('h2', text: card.name)  }
    it { expect(rendered).to have_selector('p', text: card.description)  }
    it { expect(rendered).to have_selector('h4', text: 'Checklist')  }
    it { expect(rendered).to have_selector('a', text: 'add checklist')  }
    it { expect(rendered).to have_selector('a', text: 'remove')  }
    it { expect(rendered).to have_selector('a', text: 'back')  }
    it { expect(rendered).to have_selector('a', text: 'delete')  }
    it { expect(rendered).to have_selector('a', text: 'close')  }
  end

  describe 'card without checklist' do
    before do
      card
      assign(:card, CardPresenter.new(card))
      render
    end

    it { expect(view).to render_template(partial: '_new_checklist')  }
  end
end