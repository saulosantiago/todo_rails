require 'spec_helper'

describe 'lists/new.html.erb', type: :view do
  let(:user) { create(:user) }
  let(:list) { build(:list, user: user) }

  before do
    sign_in(user)
    assign(:list, list)
    render
  end

  it { expect(view).to render_template(partial: '_form')  }
  it { expect(view).to render_template(partial: '_card_form')  }
  it { expect(rendered).to have_selector('h2', text: 'New list')  }
  it { expect(rendered).to have_selector('a', text: 'add card')  }
  it { expect(rendered).to have_selector('a', text: 'remove')  }
  it { expect(rendered).to have_selector('h3', text: 'Cards')  }
end