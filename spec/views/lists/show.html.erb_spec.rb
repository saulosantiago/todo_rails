require 'spec_helper'

describe 'lists/show.html.erb', type: :view do
  let(:user) { create(:user) }
  let(:list) { create(:list, user: user) }
  let(:card) { create(:card, list: list) }

  before do
    sign_in(user)
  end

  describe 'lists' do
    before do
      card
      assign(:list, ListPresenter.new(list))
      render
    end

    it { expect(view).to render_template(partial: '_cards_list')  }
    it { expect(rendered).to have_selector('h4', text: list.name)  }
    it { expect(rendered).to have_selector('li a', text: card.name)  }
    it { expect(rendered).to have_selector('a', text: 'edit')  }
    it { expect(rendered).to have_selector('a', text: 'back')  }
  end

  describe 'empty list' do
    before do
      list
      assign(:list, ListPresenter.new(list))
      render
    end

    it { expect(view).to render_template(partial: 'shared/_warning')  }
    it { expect(rendered).to have_selector('.alert.alert-warning', text: 'There is nothing registered')  }
    it { expect(rendered).to have_selector('a', text: 'edit')  }
    it { expect(rendered).to have_selector('a', text: 'back')  }
  end
end