require 'spec_helper'

describe 'lists/index.html.erb', type: :view do
  let(:user) { create(:user) }
  let(:list) { create(:list, user: user) }

  before do
    sign_in(user)
  end

  describe 'lists' do
    before do
      list
      assign(:lists, ListsPresenter.new(List.publics(User.last)))
      render
    end

    it { expect(view).to render_template(partial: '_list')  }
    it { expect(rendered).to have_selector('.is_my')  }
    it { expect(rendered).to have_selector('a.btn.btn-primary', text: 'new list')  }
    it { expect(rendered).to have_selector('h2 small', text: list.name)  }
    it { expect(rendered).to have_selector('a.btn.btn-success', text: 'add favorite')  }
  end

  describe 'empty list' do
    before do
      assign(:lists, ListsPresenter.new(List.publics(User.last)))
      render
    end

    it { expect(view).to render_template(partial: 'shared/_warning')  }
    it { expect(rendered).to have_selector('.alert.alert-warning', text: 'There is nothing registered')  }
  end
end