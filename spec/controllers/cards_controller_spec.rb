require 'spec_helper'

describe CardsController, type: :controller do
  before do
    sign_in
  end

  let(:list) { create(:list, user: User.last) }

  describe 'show' do
    before do
      get :show, id: create(:card, list: list)
    end

    it { expect(response).to be_success }
    it { expect(response).to render_template(:show) }
    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:card).get_subject == Card.last).to be_truthy }
  end

  describe 'update' do
    let(:card) { create(:card, list: list) }

    describe 'valid' do
      before do
        post :update, id: card, card: attributes_for(:card)
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to "/cards/#{ Card.last.id }" }
    end

    describe 'invalid' do
      let(:list) { create(:list, user: User.first) }

      before do
        post :update, id: card, card: attributes_for(:card, name: nil)
      end

      it { expect(response).to have_http_status(200) }
      it { expect(response).to render_template(:show) }
      it { expect(assigns(:card).get_subject == card).to be_truthy }
    end
  end

  describe 'destroy' do
    let(:card) { create(:card, list: list) }

    before do
      delete :destroy, id: card
    end

    it { expect(response).to redirect_to "/lists/#{ card.list.id }" }
    it { expect(Card.last != card).to be_truthy }
  end

  describe 'close' do
    let(:card) { create(:card, list: list) }
    let(:checklist) { create(:checklist, card: card) }

    before do
      checklist
      get :close, id: checklist.card
    end

    it { expect(response).to have_http_status(302) }
    it { expect(response).to redirect_to "/cards/#{ card.id }" }
    it { expect(Card.last.closed_at).to be_present }
    it { expect(Card.last.closed_at).not_to be_blank }
  end
end