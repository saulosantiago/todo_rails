require 'spec_helper'

describe ListsController, type: :controller do
  before do
    sign_in
  end

  describe 'index' do
    before do
      get :index
    end

    it { expect(response).to be_success }
    it { expect(response).to render_template(:index) }
    it { expect(response).to have_http_status(200) }
  end

  describe 'new' do
    before do
      get :new
    end

    it { expect(response).to be_success }
    it { expect(response).to render_template(:new) }
    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:list)).to be_a_new(List) }
  end

  describe 'create' do
    describe 'valid' do
      before do
        post :create, list: attributes_for(:list)
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to "/lists/#{ List.last.id }" }
    end

    describe 'invalid' do
      before do
        post :create, list: attributes_for(:list, name: nil)
      end

      it { expect(response).to have_http_status(200) }
      it { expect(response).to render_template(:new) }
      it { expect(assigns(:list)).to be_a_new(List) }
    end
  end

  describe 'edit' do
    let(:list) { create(:list, user: User.first) }

    before do
      get :edit, id: list.id
    end

    it { expect(response).to be_success }
    it { expect(response).to render_template(:edit) }
    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:list) == list).to be_truthy }
  end


  describe 'update' do
    describe 'valid' do
      before do
        list = create(:list, user: User.first)
        post :update, id: list.id, list: attributes_for(:list)
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to "/lists/#{ List.last.id }" }
    end

    describe 'invalid' do
      let(:list) { create(:list, user: User.first) }

      before do
        post :update, id: list.id, list: attributes_for(:list, name: nil)
      end

      it { expect(response).to have_http_status(200) }
      it { expect(response).to render_template(:edit) }
      it { expect(assigns(:list) == list).to be_truthy }
    end
  end

  describe 'destroy' do
    let(:list) { create(:list, user: User.last) }

    before do
      delete :destroy, id: list
    end

    it { expect(response).to redirect_to "/lists" }
    it { expect(List.last != list).to be_truthy }
  end

  describe 'favorite' do
    let(:list) { create(:list, user: User.last) }

    describe 'add' do
      before do
        get :favorite, id: list
      end

      it { expect(List.favorites(User.last).length == 1).to be_truthy }
      it { expect(response).to redirect_to "/lists" }
    end

    describe 'del' do
      before do
        create(:favorite_list, list: list, user: User.last)
        get :favorite, id: list
      end

      it { expect(List.favorites(User.last).length == 0).to be_truthy }
      it { expect(response).to redirect_to "/lists" }
    end
  end

  describe 'favorites' do
    let(:list) { create(:list, user: User.last) }

    before do
      create(:favorite_list, list: list, user: User.last)
      get :favorites
    end

    it { expect(response).to render_template(:favorites) }
    it { expect(assigns(:lists).get_subject == List.favorites(User.last)).to be_truthy }
  end
end