require 'spec_helper'

feature 'Visitor card show', js: true do
  let(:user) { User.last }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }
  let(:checklist) { create(:checklist, card: card, resolved: false) }

  before do
    sign_in_as_a_user
    checklist
    visit card_path(card)
  end

  scenario 'add checklist' do
    click_link 'add checklist'
    fill_in 'description', with: 'foozin'
    click_button 'save'

    expect(page).to have_content('foozin')
  end

  describe 'when all checklist resolved' do
    before do
      check 'card[checklists_attributes][0][resolved]'
      click_button 'save'
    end

    scenario { expect(all('span.checked').size).to be_eql(1) }
    scenario { expect(all('span.label-warning').size).to be_eql(1) }
  end

  describe 'delete card' do
    before { click_link 'delete' }
    scenario { expect(page).to have_content('There is nothing registered') }
    scenario { expect(page).to have_content('foo') }
  end
end