require 'spec_helper'

describe 'new list', js: true do
  before do
    sign_in_as_a_user
    visit lists_path
    click_link 'new list'
  end

  feature 'Visitor new list with card' do
    let(:list) { attributes_for(:list) }

    before do
      fill_form_list_with_card(list)
    end

    scenario { expect(page).to have_content(list[:name]) }
    scenario { expect(page).to have_content('foo') }
  end

  feature 'Visitor new list with two cards' do
    let(:list) { attributes_for(:list) }

    before do
      fill_form_list_with_cards(list)
    end

    scenario { expect(page).to have_content(list[:name]) }
    scenario { expect(page).to have_content('foo') }
    scenario { expect(page).to have_content('bar') }
  end

  feature 'Visitor new list with checklist' do
    let(:list) { attributes_for(:list) }

    before do
      fill_form_list_without_card(list)
    end

    scenario { expect(page).to have_content(list[:name]) }
    scenario { expect(page).to have_content('There is nothing registered') }
  end
end