require 'spec_helper'

feature 'Visitor list show' do
  let(:user) { User.last }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }

  before do
    sign_in_as_a_user
    card
    visit list_path(list)
  end

  scenario { expect(page).to have_content('foo') }
  scenario { expect(page).to have_content(list.name) }
end