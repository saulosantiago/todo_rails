require 'spec_helper'

feature 'Visitor list edit' do
  let(:user) { User.last }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }

  before do
    sign_in_as_a_user
    card
    visit list_path(list)
  end

  describe 'when button edit clicked' do
    before { click_link 'edit' }

    scenario { expect(page).to have_content("Edit list: #{ list.name }") }
  end

  describe 'when card removed', js: true do
    before do
      click_link 'edit'
      click_link 'remove'
      click_button 'save'
    end

    scenario { expect(page).to have_content('foo') }
    scenario { expect(page).to have_content('There is nothing registered') }
  end

  describe 'when delete list' do
    before do
      click_link 'edit'
      click_link 'delete'
    end

    scenario { expect(page).to have_content('new list') }
  end
end