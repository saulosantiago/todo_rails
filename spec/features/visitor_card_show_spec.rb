require 'spec_helper'

feature 'Visitor card show' do
  let(:user) { User.last }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }
  let(:checklist) { create(:checklist, card: card) }

  before do
    sign_in_as_a_user
    checklist
    visit card_path(card)
  end

  scenario { expect(page).to have_content(card.name) }
  scenario { expect(page).to have_content(card.description) }
  scenario { expect(page).to have_content(checklist.description) }
end