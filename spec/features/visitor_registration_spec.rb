require 'spec_helper'

feature 'Visitor registration' do
  let(:user) { attributes_for(:user) }

  scenario 'with valid email and password' do
    sign_up_with user[:email], '123123'
    expect(page).to have_content('sign out')
  end

  scenario 'with invalid email' do
    sign_up_with 'invalid_email', 'password'
    expect(page).to have_content('Sign up')
  end

  scenario 'with blank password' do
    sign_up_with user[:email], ''
    expect(page).to have_content('Sign up')
  end
end