require 'spec_helper'

feature 'Visitor login' do
  let(:user) { create(:user) }

  scenario 'with valid email and password' do
    sign_in_with user.email, '123123'
    expect(page).to have_content('sign out')
  end

  scenario 'with invalid email' do
    sign_up_with 'invalid_email', 'password'
    expect(page).to have_content('Log in')
  end

  scenario 'with blank password' do
    sign_up_with user.email, ''
    expect(page).to have_content('Log in')
  end
end