require 'spec_helper'

feature 'Visitor favorite list' do
  let(:user) { create(:user) }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }

  before do
    card
    sign_in_with user.email, '123123'
  end

  scenario 'add to my favorites list' do
    click_link 'add favorite'
    expect(page).to have_content('del favorite')
  end

  scenario 'remove to my favorites list' do
    click_link 'add favorite'
    click_link 'del favorite'
    expect(page).to have_content('add favorite')
  end
end