require 'spec_helper'

feature 'Visitor lists' do
  let(:user) { create(:user) }
  let(:list) { create(:list, name: 'foo', user: user) }
  let(:card) { create(:card, list: list) }

  before do
    card
    sign_in_with user.email, '123123'
  end

  scenario { expect(page).to have_content('Signed in successfully.') }
  scenario { expect(page).to have_content('new list') }
  scenario { expect(page).to have_content('foo') }
  scenario { expect(page).to have_content('1') }
  scenario { expect(page).to have_content('add favorite') }

  scenario 'click the button to add a new list' do
    click_link 'new list' do
      expect(page).to have_content('Create list')
    end
  end

  scenario 'click the title list' do
    click_link 'foo' do
      expect(page).to have_content('foo')
      expect(page).to have_content('back')
    end
  end
end