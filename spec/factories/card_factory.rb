FactoryGirl.define do
  factory :card do |f|
    list
    f.name Faker::App.name
    f.description Faker::Lorem.sentence(3, false, 4)
  end
end