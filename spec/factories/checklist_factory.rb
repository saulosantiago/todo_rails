FactoryGirl.define do
  factory :checklist do |f|
    card
    f.resolved { [true, false].sample }
    f.description Faker::Lorem.sentence(3, false, 4)
  end
end